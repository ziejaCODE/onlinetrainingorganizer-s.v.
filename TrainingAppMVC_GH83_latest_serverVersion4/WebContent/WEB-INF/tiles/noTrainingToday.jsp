<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>





<div class="homePage container">
	<div class="row">
		<div id="noTrainToday" class="col-xs-offset-0 col-xs-12 col-sm-offset-2 col-sm-8">
			
			<h2><a  style="text-decoration:none; color:white">  <!--  <c:out  value="${user.userName}"></c:out> -->  </a> You have no training set for today</h2>

			<p />
			<br>
			<sec:authorize access="isAuthenticated()">
				<a href="<c:url value='/createTraining'/>">Create new Training</a>
			</sec:authorize>

			<p />

			<sec:authorize access="isAuthenticated()">
				<a href="${pageContext.request.contextPath}/showUsersTrainings">Use one of your's training's</a>
			</sec:authorize>


			<p />

			<sec:authorize access="isAuthenticated()">
				<a href="<c:url value='/showAllTrainings'/>">Use one of ready training's</a>
			</sec:authorize>




			<p />

			<sec:authorize access="isAuthenticated()">
				<a href="<c:url value='/homePage'/>">Go back to home page</a>
			</sec:authorize>


		</div>


	</div>
</div>






