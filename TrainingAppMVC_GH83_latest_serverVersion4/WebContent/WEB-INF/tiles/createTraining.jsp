<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


		<script type="text/javascript">
			
					
			var tableNu = 0;
			//var ListOfExercesesNames = [];
			
			function onReady(){
				
				setWeekDays(training);	
				setExerciseNames();
				
				tableNu++;
				presentExercise(tableNu);
				
				document.getElementById("saveAndExitTrainingButton").disabled = true;
			}
			
			
			// this function creates select tag populated with days of the week options
			// and sets sets apropriate day selected according to training
			function setWeekDays(training){			
				
				//alert("Set the day ");	
				
				var trainName = document.getElementById("weekDay");
				
				var ListOfWeekDays = [ "Select day", "Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
				
				for(var i = 0; i < ListOfWeekDays.length; i++){
					
					var elemSeleList = document.createElement("option");
					var name = ListOfWeekDays[i];
					//alert("week day is now " + name);
					if((training != null) && (name == training.weekDay) ){
						//alert("exercise is now " + exercise.nameOfExercise + " and name is " + name);
						elemSeleList.innerHTML = training.weekDay;
						elemSeleList.setAttribute("value", training.weekDay);
						elemSeleList.setAttribute("selected", "selected")
					}else{
						elemSeleList.innerHTML = name;
						elemSeleList.setAttribute("value", name);							
					}				
					trainName.appendChild(elemSeleList);				
				}
			}
			
			
			function setExerciseNames(){				
				
				ListOfExercesesNames = JSON.parse(document.getElementById("exeNamesReceived").innerHTML);
				defineVaraibles(ListOfExercesesNames);
			
			}
			
			$(document).ready(onReady);			
			
			function onClick() {				
				tableNu++;
				presentExercise(tableNu);			
			}
		
			
			
			
			
			
			function saveTrainingToDatabases() {
		
				
				addExercise();
				
				var training = passTraining();
		
				//alert("training name is " + training.trainingName);
				//alert("weet day is " + training.weekDay);
		
				var ListOfExercesesNames = JSON.parse(document.getElementById("exeNamesReceived").innerHTML);
		
				//alert("training name is " + training.trainingName);
				
				if(document.getElementById("userNameHeader").innerHTML != "public"){
					training.trainingName = training.trainingName + "_" + document.getElementById("userNameHeader").innerHTML;
				}
				
				
				
				//alert("training name is " + training.trainingName);
				
				
				var myJSON = JSON.stringify(training);		
		
				$.ajax({
					type : 'POST',
					url : "${pageContext.request.contextPath}/doCreate",
					contentType : 'application/json; charset=utf-8',
					data : myJSON,
					dataType : "json",
					mimeType : 'application/json'
		
				});
				
				
								
				window.location.href = "${pageContext.request.contextPath}/presentTraining?trainingName=" + training.trainingName;
				
				
			}
			
			
			
			
			function saveAndExit() {
				
				addExercise();	
				
				var training = passTraining();
		
				if(document.getElementById("userNameHeader").innerHTML != "public"){
					training.trainingName = training.trainingName + "_" + document.getElementById("userNameHeader").innerHTML;
				}
				
				var myJSON = JSON.stringify(training);		
		
				$.ajax({
					type : 'POST',
					url : "${pageContext.request.contextPath}/doCreate",
					contentType : 'application/json; charset=utf-8',
					data : myJSON,
					dataType : "json",
					mimeType : 'application/json'
		
				});				
				
				//window.location.href = "/showUsersTrainings";
				
				window.location.href = "${pageContext.request.contextPath}/justReload";	
			}
			
			
			
			// this function send new Exercise Name to controller
			function saveNewExerciseNameToDatabases() {

				var exerciseName = document.getElementById("newExeName").value;
				
				//alert("To add new exercise your current change will be saved - OK?    " + exerciseName); // test

				var myJSON = JSON.stringify(exerciseName);

				$.ajax({
					type : 'POST',
					url : "${pageContext.request.contextPath}/getNewExercise",
					data : myJSON,
					dataType : "json",
					mimeType : 'application/json'
				});

				
				saveTrainingToDatabases();
				
				addExercise();
				
				
			}
			
			
			
			
			
			
			
			
		</script>



	<div class="wholeForm">

		<div class="createTrain  container">
	
			<div class="trainingHeader container">
				<div class="trainingTitle row">
					<h1 class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10">Create your own Training</h1>
					<a id="userNameHeader" style="display: none"><c:out value="${user.userName}"></c:out></a>
				</div>
					
					
					
					
				<div class="row" >				
					<p id="updateHeader" style="display: block">						
						<input id="trainingName" class="form-control col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-5"
							type="text" placeholder="Please enter training name"/> 						
						<select id="weekDay" class="form-control col-xs-offset-0 col-xs-8 col-sm-3" ></select>	
					</p>				
				</div>
		
		
		
		
		
				<p class="row" id="displayHeader" style="display: none">
					
					<a  id="trainingNameDisplayed"  class="col-xs-offset-1 col-xs-10 col-sm-6">			
						<c:out value="${training.trainingName}"></c:out>
					</a> 					
					<a id="weekDayDisplayed"	class="col-xs-offset-0 col-xs-6 col-md-3">
						<c:out value="${training.weekDay}"></c:out>
					</a>
					
				</p>
				
			</div>
		
		
		
		
		
		
			<div class="container">
				<div class="row" id="newExerciseNameInput">
					<input id="newExeName" class="form-control col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-6" type="text" placeholder="Please enter exercise name" />
					<button class="btnNewExeName btn col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-4" 
							onclick="saveNewExerciseNameToDatabases()">Add New Exercise Name
					</button>
				</div>
			</div>
	
			
		
		
			<div id=presentTable class="container"></div>
			
			<div id="presentTraining" class="container"></div>
			
			
			
			
			
			
			<div class="container">	
			
				<div class="row" id="d" style="display: none">
					<div class="editTraining col-xs-offset-0 col-xs-5" >
							<button class="editTrainingButton btn "
							onclick="addExercise()"	>View Training</button>
					</div>
					<div class="saveTraining col-xs-offset-0 col-xs-5"  >
							<button id="saveAndExitTrainingButton" class="saveTrainingButton  btn"
								    onclick="saveAndExit()">Save and Exit</button>
					</div>			
				</div>
				
							
				<div class="row" id="e" style="display: block">				
					<div  class="editTraining col-xs-offset-0 col-xs-5" >
							<button class="editTrainingButton btn "
							onclick="addExercise()"	>Edit Training</button>
					</div>
					<div class="saveTraining col-xs-offset-0 col-xs-5"  >
							<button id="saveAndExitTrainingButton" class="saveTrainingButton  btn"
								onclick="saveAndExit()">Save and Exit</button>
					</div>			
				</div>			
			</div>
			
			
			
			
		
			<div class="container">
				<div class="goBackLink row">
					<sec:authorize access="isAuthenticated()">
						<p class="goBack col-xs-offset-1 col-xs-10">
							<a href="<c:url value='/homePage'/>">Go back to home page</a>
						</p>
					</sec:authorize>
				</div>
			</div>
			
		
			<p id="exeNamesReceived" style="display: none"> <c:out value="${names}"></c:out></p>	
	
		</div>
	</div>
	
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/scripts/changePos11.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/scripts/displayWorking.js"></script>





