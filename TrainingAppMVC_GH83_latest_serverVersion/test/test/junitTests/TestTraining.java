package test.junitTests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import model.Exercise;
import model.Set;
import model.Training;
import model.TrainingDao;
import model.User;


@ActiveProfiles("dev")
@ContextConfiguration(locations = {
		"classpath:configs/model-context.xml", 
		"classpath:configs/security-context.xml",
		"classpath:test/config/testData.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TestTraining {
	
//	@Autowired
//	private UserDao userDao;
	
	@Autowired	
	private TrainingDao trainingModel;
	
//	@Autowired
	//private TrainingService trainingService;
	
	User user;
	Training training;
	
	@Before
	public void init(){			
		
		user = new User();
		user.setUserName("lolo");
		
		training = new Training();
		training.setUserName(user);
		training.setWeekDay("friday");
		training.setTrainingName("bieganie po polu_lolo");
		
		Exercise exercise = new Exercise();
		exercise.setNameOfExercise("bieg na boso");
		
		List<Exercise> exercises = new ArrayList<>();		
		
		Set set = new Set();
		set.setSetNo(1);
		set.setReps(10);
		set.setWeight(25);
		
		List<Set> sets = new ArrayList<>();
		sets.add(set);
		
		exercise.setSets(sets);
		
		exercises.add(exercise);
		
		training.setExercises(exercises);
	}
	
		
	
	@Test
	public void createTrainingTest(){	
		
		assertTrue("Training created true", trainingModel.createTraining(training));
	}
	
	
	@Test
	public void trainingExistsTest(){		
		
		assertTrue("Training exists true", trainingModel.trainingForDayExists(user.getUserName(), training.getWeekDay()));		
		assertTrue("Training exists true", trainingModel.trainingExists(user.getUserName(), training.getTrainingName(), training.getWeekDay()));		
		assertTrue("Training exists true", trainingModel.trainingExists(user.getUserName(), training.getTrainingName()));
		
	}
	
	
	@Test
	public void deleteTrainingTest(){	
		
		assertTrue("Training deleted true", trainingModel.deleteTraining(user.getUserName(), training.getTrainingName()));
	}
	
	
	
	
	
	
	
	
}	
	
	
	
	
	
	