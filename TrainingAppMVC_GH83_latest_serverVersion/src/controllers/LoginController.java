package controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.EmailMessage;
import model.Training;
import model.User;
import service.UserService;

@Controller
public class LoginController {
	
	
	private UserService userService;
	
	@Autowired
	public void setUserService(UserService userService){
		this.userService = userService;
	}
	
	@Autowired
	private MailSender mailSender;
	
//	@Autowired
//	public void setMailSender(MailSender mailSender) {
//		this.mailSender = mailSender;
//		System.out.println("mailSender is set");
//	}
	
	
	//////////// login stuff ///////////////////////////	
	
	@RequestMapping(value="/logIn")
	public String logIn(){
		return "logIn";
	}
	
	
	@RequestMapping(value="/accessDenied")
	public String accessDenied(){		
		return "accessDenied";
	}
	
	@RequestMapping(value="/logOut")
	public String logOut(){		
		return "logIn";
	}	
	////////////////////////////////////////////////////
	
	
	
	
	
	
	//////////// create account stuff ///////////////////////////
	
	@RequestMapping(value="/createNewAccount")
	public String createAccount(Model model){
		
		model.addAttribute("user", new User());		
		return "createNewAccount";
	}
	
	@RequestMapping(value="/doCreateAccount", method=RequestMethod.POST)
	public String accountCreated(@Valid User user, BindingResult res){
		
		if(res.hasErrors()){
			return "createNewAccount";
		}
		
		user.setAuthority("ROLE_USER");
		user.setEnabled(true);			
						
		try {
			userService.create(user);
		} catch (DuplicateKeyException e) {
			res.rejectValue("userName", "DuplicateKey.user.userName", "this user already exists");
			return "createNewAccount";
		}
			//System.out.println(user.toString());			
			return "accountCreated";
	}	
	///////////////////////////////////////////////////////////////////////	
	
	
	
	
	
	
	//////////// administrator stuff ///////////////////////////
	
	@RequestMapping("/adminPage")
	public String showAdminPage(Model model){		
		
		//throw new AccessDeniedException("Wrong");
		
		try {
			return "adminPage";
		} catch (AccessDeniedException e) {
			return "accessDenied";
		}
	}
	
	
	@RequestMapping(value="/createUser")
	public String createUser(Model model){
		
		model.addAttribute("user", new User());		
		return "createUser";
	}
	
	
	
	@RequestMapping(value="/doCreateUser", method=RequestMethod.POST)
	public String userCreated(@Valid User user, BindingResult res){
		
		if(res.hasErrors()){
			return "createNewAccount";
		}
		
		user.setAuthority("ROLE_USER");
		user.setEnabled(true);			
						
		try {
			userService.create(user);
		} catch (DuplicateKeyException e) {
			res.rejectValue("userName", "DuplicateKey.user.userName", "this user already exists");
			return "createNewAccount";
		}
			//System.out.println(user.toString());			
			return "accountCreated";
	}
	
	
	@RequestMapping("/showUsers")
	public String showUsersPage(Model model){
		
		try {
			List<User> users =  userService.getAllUsers();
			//System.out.println(users.toString()); // test to remove	
			
			model.addAttribute("users", users);
			
		} catch (AccessDeniedException e) {
			System.out.println("Exception: " + e.getClass());
			return "accessDenied";
		}
		
		return "showUsers";
	}
	
	///////////////////////////////////////////////////////////
	
	
	
	
	//////////// mail dispatcher ///////////////////////////
	
	
	@RequestMapping(value="/contact")
	public String createEmil(Model model){
		
		model.addAttribute("emailMessage", new EmailMessage());			
		return "contact";
	}
	
	
	
	
	@RequestMapping(value="/sendmessage", method=RequestMethod.POST)	
	public String sendMessage(EmailMessage emailMessage) {
		
		System.out.println("send message start");
		
		String name = emailMessage.getName();
		String surname = emailMessage.getSurname();
		String email = emailMessage.getEmail();
		String message = emailMessage.getMessage();
				
		System.out.println(emailMessage.toString());

		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setFrom("ziejacode@gmail.com");
		mail.setTo("ziejacode@gmail.com");		
		mail.setSubject("Message from " + name  + " " + surname + ": ");
		mail.setText("This message is sent from " + email + "\n \n " + message);
		
		try {
			mailSender.send(mail);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Can't send message");
		}
		
		return "messageSent";
	}	
	
	///////////////////////////////////////////////////////////	
}
	
	
	
	
	
