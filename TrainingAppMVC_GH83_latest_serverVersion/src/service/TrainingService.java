package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import model.Training;
import model.TrainingDao;


@Service("trainingService")
public class TrainingService {
	
private TrainingDao trainingModel;
	
	@Autowired
	public void setTrainingDao(TrainingDao trainingModel) {
		this.trainingModel = trainingModel;
	}

	
	public List<Training> getTrainigs(){		
		return trainingModel.getTrainings();		
	}	
	
	public Training getTodaysTraining(String userName, String weekDay){	
		return trainingModel.getTraining(userName, weekDay);		
	}

	public List<Training> showPublicTraining() {
		return trainingModel.getPublicTraining();
		
	}
	
	public List<Training> showExampleTraining() {
		return trainingModel.getExampleTraining();
		
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	public void createTraining(Training training) {		
		trainingModel.createTraining(training);		
	}


	public boolean trainingExists(String username, String trainingName) {
		return trainingModel.trainingExists(username, trainingName);		
	}

	public boolean trainingExists(String userName, String trainingName, String weekDay) {
		return trainingModel.trainingExists(userName, trainingName, weekDay);	
	}

	public List<Training> getUsersTrainings(String userName) {
		return trainingModel.getUsersTrainings(userName);
	}


	public Training presentTraining(String username, String trainingName) {
		return trainingModel.presentTraining(username, trainingName);		
	}


	public boolean deleteTraining(String username, String trainingName) {
		return trainingModel.deleteTraining(username, trainingName);	
		
	}


	public void updateTraining(Training training, Training safeCopy) {
		trainingModel.updateTraining(training, safeCopy);	
		
	}


	public List<String> getAllExercisesNames(String userName) {
		return trainingModel.getAllExercisesNames(userName);	
	}


	public void addExercisesName(String exerciseName, String userName ) {
		trainingModel.addExercisesName(exerciseName, userName);
		
	}


	public boolean trainingForDayExists(String username, String weekDay) {
		return trainingModel.trainingForDayExists(username, weekDay);
	}


	

}
