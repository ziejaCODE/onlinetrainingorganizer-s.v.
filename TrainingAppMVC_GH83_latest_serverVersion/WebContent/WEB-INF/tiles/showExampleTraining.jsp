<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>


<script type="text/javascript">
	
	function onReady() {

		var b1 = document.getElementsByClassName('editTrainingButton');
		var b2 = document.getElementsByClassName("saveTrainingButton").disabled = true;

		if (b2.disabled === true) {

		} else {
			b1.disabled === false;
		}

		var training = JSON.parse(document.getElementById("trainingReceived").innerHTML);

		setExerciseNames();
		setExercises(training);
		setWeekDays(training);

		addExercise();
		//addExercise();
		
		alert("training name is " + training.trainingName);
		
		

		
	}

	$(document).ready(onReady);

	function setExercises(training) {

		var exercises = training.exercises;
		for (var i = 0; i < exercises.length; i++) {
			var exercise = exercises[i];
			presentExercise(i, exercise);
		}
		;
	}

	function setWeekDays(training) {

		var trainName = document.getElementById("weekDay");

		var ListOfWeekDays = [ "Monday", "Tuesday", "Wednesday", "Thursday",
				"Friday", "Saturday", "Sunday" ];

		for (var i = 0; i < ListOfWeekDays.length; i++) {

			var elemSeleList = document.createElement("option");
			var name = ListOfWeekDays[i];
			//alert("week day is now " + name);
			if ((training != null) && (name == training.weekDay)) {
				//alert("exercise is now " + exercise.nameOfExercise + " and name is " + name);
				elemSeleList.innerHTML = training.weekDay;
				elemSeleList.setAttribute("value", training.weekDay);
				elemSeleList.setAttribute("selected", "selected")
			} else {
				elemSeleList.innerHTML = name;
				elemSeleList.setAttribute("value", name);
			}
			trainName.appendChild(elemSeleList);
		}
	}

	function setExerciseNames() {

		ListOfExercesesNames = JSON.parse(document
				.getElementById("exeNamesReceived").innerHTML);
		defineVaraibles(ListOfExercesesNames);
	}

	function saveTrainingToDatabases() {

		addExercise();

	}

	
	
	
	// this function send new Exercise Name to controller
	function addTrainingToUsersList() {

		alert("Training to add");

		//addExercise();

		var training = passTraining();
		
		alert("training name 1 is " + training.trainingName);

		training.trainingName = document.getElementById("trainingNameDisplayed").innerHTML + "_" + document.getElementById("userNameHeader").innerHTML;
		
		alert("training name is " + training.trainingName);
		
		//alert("user name is " + document.getElementById("userNameHeader").innerHTML);
		
		//alert("weet day is " + training.weekDay);

		//var ListOfExercesesNames = JSON.parse(document.getElementById("exeNamesReceived").innerHTML);

		//tests
		alert("training is " + training); // test	
		alert("excercises is " + training.excercises);// test

		// test obiektu
		//var  exceNames = [];
		//var  exceSets = [];

		//for(var i = 0; i < excercises.length; i++ ){
		///alert(i + " is " + excercises[i].nameOfExercise); // test
		//exceNames.push(excercises[i].nameOfExercise);
		//exceSets.push(excercises[i].sets);				
		//}

		//addExercise();

		
		
		var myJSON = JSON.stringify(training);

		$.ajax({
			type : 'POST',
			url : "${pageContext.request.contextPath}/doCreate",
			contentType : 'application/json; charset=utf-8',
			data : myJSON,
			dataType : "json",
			mimeType : 'application/json'

		});
		alert("...saved");
		resetForm();

		window.location.href = "/trainings/showUsersTrainings";

		alert("Training added");
	}
</script>


<div class="wholeForm">

	<div class="trainingHeader container">
		<div class="trainingTitle row">
			<h1 class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10">
				Example Training 			
				
				<a id="userNameHeader" style="text-decoration:none; color:white; display: none"><c:out value="${realUser.userName}"></c:out></a>
			</h1>
		</div>

		<div class="row">

			<p id="updateHeader" style="display: block">

				<input id="trainingName" class="form-control col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10"
					   type="text" value="${training.trainingName}" /> 
				<select
					id="weekDay" 
					class="form-control col-xs-offset-0 col-xs-8 col-sm-3">
					<option value="" selected></option>
				</select>

			</p>

		</div>

		<p class="row" id="displayHeader" style="display: none">

			<a id="trainingNameDisplayed" style="text-decoration:none; color:white"	
				class="col-xs-offset-1 col-xs-10 col-sm-6"> <c:out value="${training.trainingName}"></c:out>
			</a> 
			<a id="weekDayDisplayed" style="display: none"	 class="col-xs-offset-0 col-xs-6 col-md-3">
			</a>
			
		</p>

	</div>
	<div class="container">
			<div class="row" id="newExerciseNameInput" style="display: none">
				<input id="newExeName" class="form-control col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-6" type="text" placeholder="Please enter exercise name" />
				<button class="btnNewExeName btn col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-4" 
						onclick="saveNewExerciseNameToDatabases()">Add New Exercise Name
				</button>
			</div>
	</div>
	
	<div id="presentTable" class="container"></div>

	<div id="presentTraining" class="container"></div>

	
		<div class="container">		
			<div class="row" id="d" style="display: block">
				<div class="editTraining col-xs-offset-0 col-xs-5" >
						<button class="editTrainingButton btn "
							onclick="addExercise()">View Training</button>
					</div>
				<div class="saveTraining col-xs-offset-0 col-xs-5"  >
						<button id="saveAndExitTrainingButton" class="saveTrainingButton  btn"
							onclick="saveTrainingToDatabases()">Save Training</button>
				</div>		
			</div>
			<div class="row" id="e" style="display: none">			
				<div class="addTrainingToUsersList col-xs-offset-1 col-xs-6">
						<button class="saveTrainingButton  btn"
							onclick="addTrainingToUsersList()">Add to your personal
							training's</button>
					</div>		
			</div>
		</div>

		
	
		


	<div class="container" >
		<div class="goBackLink row">
			<sec:authorize access="!isAuthenticated()">
				<p class="goBack col-xs-offset-1 col-xs-10">
					<a href="<c:url value='/showTrainingExamples'/>">Go back</a>
				</p>
			</sec:authorize>
			<sec:authorize access="isAuthenticated()">
				<p class="goBack col-xs-offset-1 col-xs-10">
					<a href="<c:url value='/showAllTrainings'/>">Go back</a>
				</p>
			</sec:authorize>
		</div>
	</div>

	<p id="trainingReceived" style="display: none">
		<c:out value="${json}">
		</c:out>
	</p>
	<p id="exeNamesReceived" style="display: none">
		<c:out value="${names}"></c:out>
	</p>


	<script type="text/javascript"
		src="${pageContext.request.contextPath}/static/scripts/displayWorking.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/static/scripts/changePos11.js"></script>

</div>


















